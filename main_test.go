package main

import (
	"fmt"
	"testing"
)

func BenchmarkWalkerList(b *testing.B) {
	for i := 0; i < b.N; i++ {
		getTotalByWalking("/Users/pjcalvo/.m2")
	}
}

func BenchmarkWalkerSize(b *testing.B) {
	for i := 0; i < b.N; i++ {
		getTotalByWalkingSize("/Users/pjcalvo/.m2")
	}
}

func BenchmarkCommand(b *testing.B) {
	for i := 0; i < b.N; i++ {
		getTotalByCommand("/Users/pjcalvo/.m2")
	}
}

func BenchmarkHash(b *testing.B) {
	fmt.Println(HashDir("/Users/pjcalvo/.m3", "cache", Hash1))
}
