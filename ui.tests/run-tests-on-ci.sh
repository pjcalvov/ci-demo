#!/bin/bash

if [[ -z $AEM_AUTHOR_URL ]]; then
  export AEM_AUTHOR_URL=$ENV_AUTHOR
fi

if [[ -z $AEM_PUBLISH_URL ]]; then
  export AEM_PUBLISH_URL=$ENV_PUBLISH_0
fi

echo 'Author instance'
env | grep AEM_AUTHOR_URL

echo 'Publish instance' 
env | grep AEM_PUBLISH_URL

cd ui.tests/test-module/
npm install
npm run test-aemcloud-remote