package main

import (
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
)

func main() {
	fmt.Println(getTotalByWalking("dispatcher"))
	fmt.Println(getTotalByCommand("dispatcher"))
}

func getTotalByWalking(dir string) string {
	var total int
	err := filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			total++
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	return fmt.Sprint("Total files 1: ", total)
}

func getTotalByWalkingSize(dir string) string {
	var total int64
	err := filepath.Walk(dir,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			total += info.Size()
			return nil
		})
	if err != nil {
		log.Println(err)
	}

	return fmt.Sprint("Total files 1: ", total)
}

func getTotalByCommand(dir string) string {

	outByte, err := exec.Command("bash", "-c", "find dispatcher | wc -l").Output()

	if err != nil {
		log.Fatal(err)
	}

	out := string(outByte)

	total, err := strconv.Atoi(strings.TrimSpace(out))
	if err != nil {
		log.Fatal(err)
	}

	return fmt.Sprint("Total files 2: ", total)
}

type Hash func(files []string, open func(string) (io.ReadCloser, error)) (string, error)

func Hash1(files []string, open func(string) (io.ReadCloser, error)) (string, error) {
	h := sha256.New()
	files = append([]string(nil), files...)
	sort.Strings(files)
	for _, file := range files {
		if strings.Contains(file, "\n") {
			return "", errors.New("dirhash: filenames with newlines are not supported")
		}
		r, err := open(file)
		if err != nil {
			return "", err
		}
		hf := sha256.New()
		_, err = io.Copy(hf, r)
		r.Close()
		if err != nil {
			return "", err
		}
		fmt.Fprintf(h, "%x  %s\n", hf.Sum(nil), file)
	}
	return "h1:" + base64.StdEncoding.EncodeToString(h.Sum(nil)), nil
}

// HashDir returns the hash of the local file system directory dir,
// replacing the directory name itself with prefix in the file names
// used in the hash function.
func HashDir(dir, prefix string, hash Hash) (string, error) {
	files, err := DirFiles(dir, prefix)
	if err != nil {
		return "", err
	}
	osOpen := func(name string) (io.ReadCloser, error) {
		return os.Open(filepath.Join(dir, strings.TrimPrefix(name, prefix)))
	}
	return hash(files, osOpen)
}

// DirFiles returns the list of files in the tree rooted at dir,
// replacing the directory name dir with prefix in each name.
// The resulting names always use forward slashes.
func DirFiles(dir, prefix string) ([]string, error) {
	var files []string
	dir = filepath.Clean(dir)
	err := filepath.Walk(dir, func(file string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		rel := file
		if dir != "." {
			rel = file[len(dir)+1:]
		}
		f := filepath.Join(prefix, rel)
		files = append(files, filepath.ToSlash(f))
		return nil
	})
	if err != nil {
		return nil, err
	}
	return files, nil
}
